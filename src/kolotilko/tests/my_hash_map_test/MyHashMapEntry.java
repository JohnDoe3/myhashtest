package kolotilko.tests.my_hash_map_test;

public class MyHashMapEntry<T> {
	int key;
	T value;
	
	public MyHashMapEntry(int key_inp, T value_inp) {
		key = key_inp;
		value = value_inp;
	}
	
	@Override
	public String toString() {
		return "Key:"+String.valueOf(key)+". Value:"+String.valueOf(value);
	}

	
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}

}
