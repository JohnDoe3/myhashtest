package kolotilko.tests.my_hash_map_test.tests;

import kolotilko.tests.my_hash_map_test.MyHashMap;
import kolotilko.tests.my_hash_map_test.MyHashMapEntry;

public class MyHashMapTester {

	public static void main(String[] args) throws Exception {
	    System.out.println("Puting string and getting it back test...");
	    addString_getBackSame();
	    
	    System.out.println("Puting integer and getting it back test...");
	    addInteger_getBackSame();
	    
	    System.out.println("When can't find object -> null is returned test...");
	    getNullWhenNoSuchKey();
	    
	    System.out.println("Negative keys are alowed test...");
	    negativeKeysDontFail();
	    
	    System.out.println("Auto-increase test...");
	    add_autoIncrease_GetSameObjects();
	    
	    System.out.println("Manual increase test...");
	    add_manualIncrease_GetSameObjects();
	    
	    System.out.println("Removing non-existing element test...");
	    removeNonExistentElem_DontFail();
	    
	    System.out.println("Removing added element test...");
	    addElement_removeElement_getNullByElementKey();
	    
	    System.out.println("Getting right element after collision test...");
	    makeCollision_getRightElement();
	    
	    System.out.println("Done");
	    
	    /*
        //linear case. Make printState public to check it out
        MyHashMap<String> hashMap = new MyHashMap<String>(2);
        hashMap.add(1, "One");
        hashMap.printState();
        hashMap.add(2, "Two");
        hashMap.printState();
        hashMap.add(3, "Three");
        hashMap.printState();
        hashMap.add(4, "Four");
        hashMap.printState();
        hashMap.add(5, "Five");
        hashMap.printState();
        //collision with 1 (size is 8). Can comment out adding 1 operation to see it
        //+can comment out others to see how it takes other places  
        hashMap.add(9, "Nine");
        hashMap.printState();
        String result = "Got back element: " + hashMap.get(9);
        System.out.println(result);
        //*/
	}
	
	static void makeCollision_getRightElement() throws Exception {
        MyHashMap<String> hashMap = new MyHashMap<String>(8);
        hashMap.add(1, "One");
        hashMap.add(9, "Nine");
        String result = hashMap.get(9);
        if (!"Nine".equals(result)) {
        	throw new Exception("Get back wrong result after collision");
        }
	}
	
	static void addElement_removeElement_getNullByElementKey() throws Exception {
        MyHashMap<Double> hashMap = new MyHashMap<Double>();
        hashMap.add(3, 13.0);
        hashMap.remove(3);
        if (hashMap.get(3)!=null) {
        	throw new Exception("Element was not removed");
        }
	}
	
	static void removeNonExistentElem_DontFail() throws Exception {
        MyHashMap<Byte> hashMap = new MyHashMap<Byte>();
        hashMap.remove(255);
	}
	
	static void add_manualIncrease_GetSameObjects() throws Exception {
        MyHashMap<String> hashMap = new MyHashMap<String>(2);
        hashMap.increase(2);
        hashMap.add(1, "One");
        hashMap.add(2, "Two");
        hashMap.add(3, "Three");
        if (!"One".equals(hashMap.get(1))) {
            throw new Exception("Get after manual increase failed (1)");
        }
        if (!"Two".equals(hashMap.get(2))) {
            throw new Exception("Get after manual increase failed (2)");
        }
        if (!"Three".equals(hashMap.get(3))) {
            throw new Exception("Get after manual increase failed (3)");
        }
	}
	
	static void add_autoIncrease_GetSameObjects() throws Exception {
        MyHashMap<String> hashMap = new MyHashMap<String>(2);
        hashMap.add(1, "One");
        hashMap.add(2, "Two");
        hashMap.add(3, "Three");
        if (!"One".equals(hashMap.get(1))) {
            throw new Exception("Get after auto increase failed (1)");
        }
        if (!"Two".equals(hashMap.get(2))) {
            throw new Exception("Get after auto increase failed (2)");
        }
        if (!"Three".equals(hashMap.get(3))) {
            throw new Exception("Get after auto increase failed (3)");
        }
	}
	
	static void negativeKeysDontFail() throws Exception {
        MyHashMap<String> hashMap = new MyHashMap<String>();
        hashMap.add(-1, "Negative");
	    addString_getBackSame();
	}
	
	static void getNullWhenNoSuchKey() throws Exception {
        MyHashMap<String> hashMap = new MyHashMap<String>();
        hashMap.add(0, "noiseKey");
        Object storedObject = hashMap.get(1);
        if (storedObject!=null ) {
            throw new Exception("Recieved object for a not existing key");
        }
	}	
	
	static void addInteger_getBackSame() throws Exception{
		MyHashMap<Integer> hashMap = new MyHashMap<Integer>();
		int testKey = 23;
		Integer testValue = new Integer(6);
        hashMap.add(testKey, testValue);        
        addEntry_getObjectBack(new MyHashMapEntry<Integer>(testKey, testValue));
	}
	
	static void addString_getBackSame() throws Exception {
        MyHashMap<String> hashMap = new MyHashMap<String>();
        
        int testKey = 15;
        String testValue = "Test";
        
        hashMap.add(testKey, testValue);
        
        String storedValue = hashMap.get(testKey);
        
        String storedValueString = String.valueOf(storedValue);
        if (! testValue.equals(storedValueString)) {
            String errorMessage = 
                    String.format("Returned string is not equal to added. Expected:%s , recieved:%s", 
                    testValue, storedValueString);
            throw new Exception(errorMessage);
        }
	}

	
	static <T> void addEntry_getObjectBack(MyHashMapEntry<T> entry) throws Exception {
        MyHashMap<T> hashMap = new MyHashMap<T>();
        
        hashMap.add(entry.getKey(), entry.getValue());
        
        Object storedValue = hashMap.get(entry.getKey());
        
        if (!entry.getValue().equals(storedValue) ) {
            throw new Exception("Returned object is not equal to added");
        }
	}
	
}
