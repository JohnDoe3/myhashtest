package kolotilko.tests.my_hash_map_test;

public class MyHashMap<T> {
	
	static final int DEFAULT_SIZE = 100;
	static final int DEFAULT_INCREASE_MULTIPLIER = 2;
	
	Object lock = new Object();
	
	MyHashMapEntry<T>[] elements;
	int elementsUsed;
	public MyHashMap() {
		this(DEFAULT_SIZE);
	}
	
	public MyHashMap(int size_inp) {
		if (size_inp<1) size_inp = 1;
		elements = new MyHashMapEntry[size_inp];
	} 	
	
	/**
	 * Add key-value pair. If there is not enough space -> gets more space
	 * @param key
	 * @param value
	 */
	public void add(int key, T value) {
		synchronized (lock) {
	        //check if there is space for another element
			if (elementsUsed == elements.length) {
			    increaseRaw(DEFAULT_INCREASE_MULTIPLIER);
			}	
			
			//add new entry
			addRaw(key, value);  
		}
	}
	
	void addRaw(int key, T value) {
        int hashedIndexStart = getHashByKey(key);
        int hashedIndexNow = hashedIndexStart;
        //look for empty cell. Found->write->exit
        do {
            if (elements[hashedIndexNow]==null) {
                //empty slot - can use it
                elements[hashedIndexNow] = new MyHashMapEntry(key, value);
                elementsUsed++;
                return;
            }
            else {
                //occupied. Need next variant
	            hashedIndexNow=nextIndexRaw(hashedIndexNow,key);
            }
            
        } while (hashedIndexNow != hashedIndexStart);//full circle. Breaking out
	}
	
	/**
	 * Returns object associated with this key or null
	 * @param key to look for
	 * @return associated object or null
	 */
	public T get(int key) {		
		//if hash points not to null, then it MAY be element we are looking for
		//but if it points to null -> key-value pair is not present
	    synchronized (lock) {
	        int hashedIndexStart = getHashByKey(key);
	        int hashedIndexNow = hashedIndexStart;
	        do {
	            if (elements[hashedIndexNow]==null) return null;

	            MyHashMapEntry<T> entry = elements[hashedIndexNow];
	            if (entry.getKey() == key) {
	                return entry.getValue(); 
	            }
	            
	            hashedIndexNow=nextIndexRaw(hashedIndexNow,key); 
	            
	        } while (hashedIndexNow != hashedIndexStart);
	        
	        //browsed till overlapped back to same element -> not found
	        return null;
        }
	}
	
	/**
	 * Remove key-value pair from map. If key is not in map -> nothing happens
	 * @param key to look up for further removal
	 */
	public void remove(int key) {
	    synchronized (lock) {      
	        int hashedIndexStart = getHashByKey(key);
	        int hashedIndexNow = hashedIndexStart;
	        do {
	            if (elements[hashedIndexNow]==null) return;//element not present

	            MyHashMapEntry<T> entry = elements[hashedIndexNow];
	            if (entry.getKey() == key) {
	                elements[hashedIndexNow] = null;//GC will take care of this element
	                elementsUsed--;
	                return;
	            }

	            hashedIndexNow=nextIndexRaw(hashedIndexNow,key);     
	            
	        } while (hashedIndexNow != hashedIndexStart);      
        }
	}
	
	/**
	 * Increase capacity of map
	 * @param multiplier by which capacity will be increased
	 */
	public void increase(int multiplier) {
		if (multiplier<2) {
			return;
		}
		
		synchronized (lock) {
		    increaseRaw(multiplier);
        }
	}
	
	void increaseRaw(int multiplier) {
        MyHashMapEntry<T>[] elementsSave = elements; //GC will take care of this
        elements = new MyHashMapEntry[elements.length*multiplier];
        elementsUsed = 0;
        for (int i=0; i<elementsSave.length; i++) {
            if (elementsSave[i]!=null) {
                addRaw(elementsSave[i].getKey(), elementsSave[i].getValue());
            }
        }
	}
	
	

	int getHashByKey(int key) {
	    return Math.abs(key%elements.length);
	}
	
	//with this we should be able to get from linear to whatever dangerous type of lookup we want fast
	//or clearly fail if index is miscalculated
	int nextIndexRaw(int hashedIndexNow, int key) {
	    hashedIndexNow+=1;
	    
        if (hashedIndexNow>=elements.length) {
            hashedIndexNow = 0;
        }                 
	    return hashedIndexNow;
	}
	
	
	void printState() {
		synchronized (lock) {
			for (int i=0; i< elements.length; i++) {
				System.out.println("Position:"+i+". Element:"+String.valueOf(elements[i]));
			}
		}
		System.out.println();
	}
	
}
